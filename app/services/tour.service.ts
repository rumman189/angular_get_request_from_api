import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()

export class TourService {

    constructor( private http:Http){
        console.log('PostService initialized....');
    }

    getTour(){
        return this.http.get('http://202.191.125.158/en/tour/3-day-bangladesh-world-heritage-tour-north-bengal')
        
        .map(res => res.json());
    }
    
}
