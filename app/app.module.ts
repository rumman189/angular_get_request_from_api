import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent }  from './app.component';
import { UserComponent }  from './components/user.component';
import {HttpModule} from '@angular/http';


@NgModule({
  imports:      [ BrowserModule, HttpModule ],
  declarations: [ AppComponent , UserComponent ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
