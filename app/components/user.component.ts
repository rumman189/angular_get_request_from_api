import { Component } from '@angular/core';
import {TourService} from '../services/tour.service';

@Component({
    selector: 'user',
    template: `

  
<h1><b>Tour Details</b></h1>
<hr>
<div class="row">
  <div class="col-xs-9">
<h3><b>Tiltle</b></h3>
<p>{{title}}</p>


<h3><b>Description</b></h3>
<p><i>{{description}}</i></p>

</div>

<div class="col-xs-5">
<h3><b>Country</b></h3>
<p>{{country}}</p>
</div>
<div class="col-xs-6">
<h3><b>Duration</b></h3>
<p>{{duration}}</p>
</div>

</div>

  <h3><b>Food</b></h3>
  
<div *ngFor="let food of foods">
<li>
<ul><i>{{food.name}}</i></ul>
</li>
</div>
<h3><b>Cancellation Policy</b></h3>

<p>{{cancel}}</p>
  `,


    providers: [TourService]
})
export class UserComponent  {


    id:number;
    title: string;
    description :string;
    country:string;
    duration:string;
    price:string;
    foods:Food[];
    cancel:string;


    constructor(private tourService :TourService){


        this.tourService.getTour().subscribe(
            tours=>{
                this.title=tours.tour.title,
                this.description=tours.tour.description,
                this.country=tours.tour.country.country,
                this.duration=tours.tour.tour_duration.duration,
                this.foods=tours.tour.foods,
                this.cancel=tours.tour.cancellation_policy;


            });
    }



}
interface Food{
   id :string;
   name:string;


}

